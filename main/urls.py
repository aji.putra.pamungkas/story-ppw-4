from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('about-me/', views.aboutme, name='aboutme'),
    path('project/', views.project, name='project'),
    path('contact/', views.contact, name='contact'),
]
