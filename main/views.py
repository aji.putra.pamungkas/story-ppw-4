from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def aboutme(request):
    return render(request, 'main/aboutme.html')

def contact(request):
    return render(request, 'main/contact.html')

def project(request):
    return render(request, 'main/project.html')
